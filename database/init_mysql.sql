create database dados;
use dados;
create table dados (id int auto_increment primary key, nome varchar(100), email varchar(100), comentario varchar(500));
commit;

create user 'exporter'@'127.0.0.1' identified by 'exporter';
grant process, replication client, select ON *.* to 'exporter'@'127.0.0.1';
commit;