variable "ami" {
	description = "Amazon Linux"
	type = string
	default = "ami-09d95fab7fff3776c"
}

variable "instance_type" {
	description = "Tipo da instancia"
	type = string
	default = "t2.xlarge"
}