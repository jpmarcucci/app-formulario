provider "aws" {
    region = "us-east-1"
    shared_credentials_file = "credentials"
    profile = "terraform"
}

resource "aws_instance" "app-formulario" {
    count = 1
    ami = var.ami
    instance_type = var.instance_type

    tags ={
        Name = "app-formulario"
    }

    vpc_security_group_ids = [aws_security_group.app-formulario.id]

    user_data = <<-EOF
        #!/bin/bash
        sleep 60
        echo "Instalando docker e GIT"
        yum install docker git -y
        echo "Iniciando Docker"
        systemctl start docker
        echo "Instalando docker-compose"
        curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose
        chmod +x /usr/bin/docker-compose
        echo "Clonando repositorio"
        git clone https://gitlab.com/jpmarcucci/app-formulario.git
        cd app-formulario
        IP=`curl -s http://169.254.169.254/latest/meta-data/public-ipv4`
        sed -i.bak "s/127.0.0.1/$IP/g" docker-compose.yml
        echo "Iniciando sistemas"
        docker-compose up -d
        EOF
}

resource "aws_security_group" "app-formulario" {
    name    =   "Security Group app-formulario"
    description = "Security Group para instancia app-formulario"

    ingress {
        from_port = 9000
        to_port = 9000
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 3000
        to_port = 3000
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_eip" "app-formulario"{
    instance= aws_instance.app-formulario[0].id
    vpc = true
}

output "IPs" {
    value = aws_eip.app-formulario.public_ip
}