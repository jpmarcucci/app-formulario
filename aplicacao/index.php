<html>
<head><title>Formulário</title></head>
<body>
<form action="/form.php" method="POST">
<table>
<tr><td><b>Nome:</b></td><td><input type="text" name="varNome" required></td></tr>
<tr><td><b>Email:</b></td><td><input type="text" name="varEmail" required></td></tr>
<tr><td><b>Comentário:</b></td><td><textarea name="varComentario" rows="10" cols="25" required></textarea></td></tr>
</table>
<input type="submit" name="Enviar" value="Enviar">
</form>

<table>
<?php
include 'functions.php';

$DB_HOST = getenv("DB_HOST");

if (!empty($DB_HOST)){
    $dbhost=$DB_HOST;
}

$conn = new mysqli($dbhost, $user, $password, $banco);

if ($conn->connect_error){
    die("Falha na conexão com o banco de dados! " . mysqli_connect_error());
}

$varSQL = "SELECT * FROM dados;";

$resultado = $conn->query($varSQL);

if ($resultado->num_rows > 0){
    print("<tr><td>Nome</td><td>Email</td><td>Comentario</td>");
    while($row=$resultado->fetch_assoc()){
        print("<tr>");
        print("<td>".$row['nome']."</td>");
        print("<td>".$row['email']."</td>");
        print("<td>".$row['comentario']."</td>");
        print("</tr>");
    }
}else{
    print("Nenhum registro encontrado!");
}

$conn->close();
?>
</table>
</body>
</html>