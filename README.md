# App-Formulário

Aplicativo web para cadastrar nome, email e comentário em banco de dados. O aplicativo roda em ambiente apache + php e grava os dados em banco de dados mysql.

Além disso acompanha um centralizador de logs (Graylog) e um sistema de métricas e dashboard (Prometheus e Grafana).

## Requisitos

- Docker
- Docker Compose
- Cluster Kubernetes (opcional, caso queira rodar em ambiente kubernetes)
- Terraform (opcional, caso queira rodar na AWS)


## Rodar local
```bash
docker-compose up -d
```

URL Aplicativo: http://localhost:8080  
URL Grafana:    http://localhost:3000  
URL Graylog:    http://localhost:9000  

Passos adicionais:

Acesse a página do Graylog com o usuário e senha admin/admin e em SYSTEM/INPUTS crie o INPUT para o SYSLOG UDP na porta 1514.

Para visualizar e criar gráficos acesse a página do Grafana com o usuário e senha admin/admin

Ao rodar o compose o grafana vai mapear o diretório grafanadb que já tem alguns gráficos criados por mim para o container do grafana.

Para finalizar os sistemas:

```bash
docker-compose down
```

## Rodar em ambiente Kubernetes

Edite o arquivo graylog.yaml no diretório kubernetes e altere a variável GRAYLOG_HTTP_EXTERNAL_URI para o IP de um nó do Kubernetes para poder acessar a página do Graylog.

```bash
kubectl create namespace app-formulario   
kubectl apply -f kubernetes/  
```  

URL Aplicativo: http://IP_do_Nó_Kubernetes:30000  
URL Grafana:    http://IP_do_Nó_Kubernetes:30002  
URL Graylog:    http://IP_do_Nó_Kubernetes:30001  

Passos adicionais:

Acesse a página do Graylog com o usuário e senha admin/admin e em SYSTEM/INPUTS crie o INPUT para o SYSLOG UDP na porta 1514.

Para visualizar e criar gráficos acesse a página do Grafana com o usuário e senha admin/admin e configure o datasource para o Prometheus no endereço http://svc-apprometheus:9090

Para finalizar os sistemas:

```bash
kubectl delete -f kubernetes/
kubectl delete namespace app-formulario
```


## Rodar na AWS

Edite o arquivo credentials dentro do diretório aws com suas credenciais da AWS.

```bash
cd aws/
terraform init
terraform plan
terraform apply
```
Acesse os sistemas com o IP informado no OUTPUT do terraform nas portas:

Porta do Aplicativo: 8080  
Porta do Grafana: 3000  
Porta do Graylog: 9000  

Acesse a página do Graylog com o usuário e senha admin/admin e em SYSTEM/INPUTS crie o INPUT para o SYSLOG UDP na porta 1514.

Para visualizar e criar gráficos acesse a página do Grafana com o usuário e senha admin/admin e configure o datasource para o Prometheus no endereço http://localhost:9090

Para finalizar os sistemas:

```bash
cd aws/
terraform destroy
```

## Documentação 

Este setup irá rodar 10 containers, sendo:
- app-formulario (Aplicativo de cadastro)
- banco-appformulario (Bando de dados MySQL onde será armazenado os dados cadastrados)
- mongo (Banco de dados MongoDB para armazenar dados do graylog)
- elasticsearch (Motor de busca para armazenar logs enviados para o graylog)
- graylog (Servidor para receber logs e armazenar no elasticsearch)
- logs (Servidor para pegar logs do Docker e enviar para graylog)
- prometheus (Servidor de métricas)
- grafana (Dashboard para métricas)
- node_exporter (Exportador de métricas do node para o prometheus)
- container_exporter (Exportador de métricas dos containers para o prometheus)

No modo docker-compose e AWS o sistema não é escalável, para isso teriamos que usar o swarm para on-premise ou algum serviço da AWS como ECS para cloud.

No caso do Kubernetes o sistema tem um HPA (HorizontalPodAutoscaler) onde após 50% de uso de CPU dos containeres é automaticamente criado um novo container para suprir a demanda.

Caso o ElasticSearch (dependencia para o Graylog) não funcionar, é necessário configurar o sistema de acordo com a documentação do mesmo:

https://www.elastic.co/guide/en/elasticsearch/reference/master/setting-system-settings.html  
https://www.elastic.co/guide/en/elasticsearch/reference/6.8/vm-max-map-count.html  